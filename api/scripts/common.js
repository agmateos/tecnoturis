'use strict';

let EJSON = require('mongodb-extended-json');


let prepareFilters = function (search = null, defaultQuery = null, defaultOptions = null, strict = false) {

	let options = defaultOptions ? defaultOptions : {};
	let query = defaultQuery ? defaultQuery : {};
	let orders = {};

	let pagination = {
		limit: 0,
		skip: 0
	}

	if (search) {
		if (typeof search == "string") search = EJSON.parse(search);		
		for (let key in search.filters) {
			if (typeof search.filters[key] == 'object') query[key] = search.filters[key];
			else if (search.filters[key] === true || search.filters[key] === false) query [key] = search.filters[key];
			else if (search.filters[key] === 'true' || search.filters[key] === 'false') query [key] = search.filters[key] == 'true' ? true : false;
			else if (typeof search.filters[key] === 'string' && isNaN (search.filters[key])) query[key] = strict ? search.filters[key] : new RegExp(wildcardToRegExp(escapeRegExp(search.filters[key])), 'i');			
			else query[key] = Number(search.filters[key]);
		}
		pagination.limit = Number(search.pagesize) || 0;
		pagination.skip = (search.page * search.pagesize) || 0;
		if (search.orders) orders = search.orders;
		if (search.project) options = search.project;
	}

	return { query: query, options: options, pagination: pagination, orders: orders };

}

let wildcardToRegExp = function (wildcard) {
	return wildcard.replace(/\*/g, '.*');
}

let escapeRegExp = function(text) {
	return text.replace(/[-[\]{}()+?.,\\^$|#\\]/g, '\\$&');
}

module.exports = {
	prepareFilters: prepareFilters
}