exports = module.exports = function(app, mongoose){

    var categorySchema = new mongoose.Schema({
        key: { type: String, required: true },
        value: { type: String, required: true },
        displayPosition: { type: Number, required: true },
        subcategories: { type: [], required: false }
    });

    mongoose.model('Category', categorySchema);
};
