exports = module.exports = function(app, mongoose){

    var productoSchema = new mongoose.Schema({
        nombre: { type: String },
        dimensiones: { 
            altura: {type: Number},
            anchura: {type: Number},
            largo: {type: Number},
        },
        codigoBarras: { type: Number },
        tags: [{ type: String }],
        estadoMaterial: { 
            categoria: {type: Number},
            listaDefectos: [{ type: String }]
        },
        unidades: {type: Number},
        img: [{ type: String }],
        category: { type: String },
        lvl1Subcategory: { type: String },
        lvl2Subcategory: { type: String },
        color: {type: String }
    });

    mongoose.model('Producto', productoSchema);
};
