//File: controllers/categories.js
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Category = mongoose.model('Category');

let common = require('./../scripts/common.js');

//GET - Return all categories in the DB
exports.findAllCategories = function (req, res) {
    try {
        queryOptions = common.prepareFilters(res.req.query.params);
        Category.find(queryOptions.query, function (err, categorias) {
            if (err) res.send(500, err.message);
            console.log('GET /categorias');
            res.status(200).jsonp(categorias);
        });
    } catch (error) {
        console.log('ERROR - findAllCategories error handler throws: ');
        console.log(error);
    }
};




