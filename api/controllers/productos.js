//File: controllers/productos.js
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Producto = mongoose.model('Producto');
let common = require('./../scripts/common.js');
var fileSystem = require('fs'),
    path = require('path');

//GET - Return all products in the DB
exports.findAllProductos = async function (req, res) {
    let queryOptions = {};
    queryOptions = common.prepareFilters(res.req.query.params);

    try {
        console.log('queryOptions: ', queryOptions);
        let productos = await Producto.find(queryOptions.query).skip(queryOptions.pagination.skip).limit(queryOptions.pagination.limit);
        console.log('Número de resultados: ', productos.length);
        res.status(200).jsonp(productos);
    } catch (error) {
        console.log('ERROR - FindAllProducts error handler throws: ');
        console.log(error);
    }
};

//GET - Return a Product with specified ID
exports.findProductoById = function (req, res) {
    try {
            Producto.findById(req.params.id, function (err, producto) {
            if (err) return res.send(500, err.message);

            console.log('GET /producto/' + req.params.id);
            res.status(200).jsonp(producto);
        });
    } catch (error) {
        console.log('ERROR - findProductoByjId error handler throws: ');
        console.log(error);
    }
};

//POST - Insert a new Product in the DB
exports.addProducto = function (req, res) {
    console.log('POST');
    console.log(req.body);
/*
    var producto = new Producto({
        title: req.body.title,
        price: req.body.price,
        provider: req.body.provider,
        rating: req.body.rating,
        img: req.body.img,
        size: req.body.size,
        downloads: req.body.downloads,
        extension: req.body.extension,
        tags: req.body.tags
    });

    producto.save(function (err, producto) {
        if (err) return res.status(500).send(err.message);
        res.status(200).jsonp(producto);
    });
    */
};

//PUT - Update a register already exists
exports.updateProducto = function (req, res) {

};

//DELETE - Delete a Producto with specified ID
exports.deleteProducto = function (req, res) {
    Producto.findById(req.params.id, function (err, producto) {
        producto.remove(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).send();
        })
    });
};

//GET - retrieves the file with specified ID
exports.downloadFile = function (req, res) {
    console.log('IniciandoDescarga');
    Producto.findById(req.params.id, function (err, producto) {
        // var ruta = producto.productFile;
        // res.status(200).jsonp(ruta);
        //res.status(200).jsonp(producto);
        //var filePath = path.join(__dirname, 'myfile.mp3');
        var ruta = producto.toObject().ruta;
        var stat = fileSystem.statSync(ruta);
        console.log(stat);

        res.writeHead(200, {
            'Content-Length': stat.size
        });
        //,
        //'Content-Type': 'audio/mpeg'
        //

        var readStream = fileSystem.createReadStream(ruta);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(res);
    });

}
