var express  = require("express"),
    app      = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    path = require('path'),
    mongoose = require('mongoose');
//27017

// CORS configuration
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Connection to DB
mongoose.connect('mongodb://localhost/atrezzo', function(err, res){
    if(err) throw err;
    console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Import Models and Controllers
var pModel = require('./models/producto')(app, mongoose);
var catefgoryModel = require('./models/category')(app, mongoose);
/*var cModel = require('./models/compra')(app, mongoose);
var uModel = require('./models/usuario')(app, mongoose);
var fModel = require('./models/filtro')(app, mongoose);
var fCModel = require('./models/filtro-cantidad')(app, mongoose);*/

var pCtrl = require('./controllers/productos');
var categoriesCtrl = require('./controllers/categories');



// API routes
var apiRouter = express.Router();


apiRouter.route('/productos')
    .get(pCtrl.findAllProductos)
    .post(pCtrl.addProducto);

apiRouter.route('/productos/:id')
    .get(pCtrl.findProductoById)
    .put(pCtrl.updateProducto)
    .delete(pCtrl.deleteProducto);




apiRouter.route('/categories')
    .get(categoriesCtrl.findAllCategories);







app.use('/api', apiRouter);

// Index Route
var router = express.Router();

router.get('*', function(req, res) {
   console.log("GET /");
   res.sendFile(path.join(__dirname, 'dist/index.html'));
});
app.use(router);

// Start server
app.listen(1991, function() {
    console.log("Node server running on http://localhost:1991");
});

