import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatSidenavModule } from "@angular/material/sidenav";
import { CategoriesService } from '../../services/categories.service'
import { ProductService } from '../../services/product.service'



@Component({
  selector: 'left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.scss']
})
export class LeftBarComponent implements OnInit {
  public title = "Menú principal";
  public currentView = "home";
  categories;
  lvl1Subcategories;
  lvl2Subcategories;
  opened = false;

  constructor(public translate: TranslateService, sidenav: MatSidenavModule, public categoriesService: CategoriesService, public productService: ProductService) { }

  ngOnInit(): void {
    this.categoriesService.getCategoriesAPI().subscribe(
      (response) => {
      this.categories = response;
        if (this.categories && this.categories.length){
          this.categoriesService.selectedCategory = this.categories[0];
          this.selectCategory( this.categoriesService.selectedCategory, 0);
        }
        this.categoriesService.categoriesChangedState.next(this.categories);
        console.log('Categorias: ', this.categories);
      },
      (err) => {
        console.log('Error on getCategories API call: ', err);
      }
    );
  }


  clearCategories() {
    this.categoriesService.clearCategories();
    this.lvl2Subcategories = null;
  }

  selectCategory(categories, level){
    if (!categories) return;
    if (categories.source && categories.value) categories = categories.value; //deberiamos mejor comprobar si es string
    if (!Array.isArray(categories)) categories = [categories];

    switch (level) {
      case 0:
        this.lvl1Subcategories = this.categoriesService.getSubcategories(categories);
        this.lvl2Subcategories = null;
        break;
      case 1:
        this.lvl2Subcategories = this.categoriesService.getSubcategories(categories);

        if (this.categoriesService.selectedLvl2Subcategories){ // If there are level 2 subcategories with unmarked parent category we remove them
          for (let category of this.categoriesService.selectedLvl2Subcategories) {

            let parent = this.categoriesService.getParentCategoryByKey(category.key, categories);
            if (!parent) {
              this.categoriesService.selectedLvl2Subcategories = this.categoriesService.selectedLvl2Subcategories.filter(function(value, index, arr){ 
                return value.key != category.key;
              });
            }
          }
        }
        break;

      case 2:
        break;
      
      default:
        break;
    }
    this.categoriesService.categoriesChangedState.next(categories);
  }

  clearFilters() {
    this.productService.leftBarFilters = {}
    this.productService.productsChangedState.next(this.productService.leftBarFilters);
  }

  updateFilters(path, value , condition = null) {
    console.log(path);
    console.log(value);
    console.log(condition);
    if (!this.productService.leftBarFilters[path]) this.productService.leftBarFilters[path] = {};
    if (condition) {
      this.productService.leftBarFilters[path][condition] = value;
    } else {
      this.productService.leftBarFilters[path] = value;
    }
    this.productService.productsChangedState.next(this.productService.leftBarFilters);
  }

}
