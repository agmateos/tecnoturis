import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product-detailed',
  templateUrl: './product-detailed.component.html',
  styleUrls: ['./product-detailed.component.scss']
})
export class ProductDetailedComponent implements OnInit {

  producto: any = {};

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService) { }

  ngOnInit(): void {
    this.route.params.subscribe(routeParams => {
      console.log(routeParams);
      if(routeParams.id){
        this.productService.getProducto(routeParams.id).subscribe(
          (response) => {
            this.producto = response;
            for (let i in this.producto.tags) if (typeof this.producto.tags[i] == 'string') this.producto.tags[i] = this.producto.tags[i][0].toLocaleUpperCase() + this.producto.tags[i].slice(1);
            this.producto.strTags = this.producto.tags.join(", ");
            console.log(this.producto)
          },
          (err) => {
            console.log('kaboom')
          }
        );
        //get producto id
      }
    })
  }

}
