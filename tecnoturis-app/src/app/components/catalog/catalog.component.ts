import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service'
import { CategoriesService } from '../../services/categories.service'
import { CommonService } from '../../services/common.service'
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {

productos = []
  constructor(private productService: ProductService, private commonService: CommonService, public categoriesService: CategoriesService) { }

  productFilters: any = {}

  ngOnInit(): void {

    this.categoriesService.categoriesChangedState.subscribe(value => {
      this.getProductos();
    })

    this.productService.productsChangedState.pipe(debounceTime(500)).subscribe(value => {
      this.getProductos();
    })
  }

  getProductos() {
    let filters = this.prepareProductFilters();

    this.productService.getProductosAPI(filters).subscribe(
      (response) => {
    this.productos = response;
      },
      (err) => {
        console.log('Error on getProducts API call');
        console.log(err);
      }
    );
  }

  prepareProductFilters() {
    let lvl1SubcategoryQuery = (this.categoriesService.selectedLvl1Subcategories && this.categoriesService.selectedLvl1Subcategories.length) ? {$in :this.commonService.getKeysOfArray(this.categoriesService.selectedLvl1Subcategories)} : null;
    let lvl2SubcategoryQuery = (this.categoriesService.selectedLvl2Subcategories && this.categoriesService.selectedLvl2Subcategories.length) ? {$in :this.commonService.getKeysOfArray(this.categoriesService.selectedLvl2Subcategories)} : null;
    let result = JSON.parse(JSON.stringify(this.productService.leftBarFilters));
    result.category = this.categoriesService.selectedCategory?.key
    result.lvl1Subcategory = lvl1SubcategoryQuery,
    result.lvl2Subcategory = lvl2SubcategoryQuery
    
    return result;
  }

}
