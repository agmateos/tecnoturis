import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })

export class CommonService {

    public removeEmpty(obj) {
        for (let key in obj) {
            if (!obj[key] && obj[key] !== 0 && obj[key] !== false) delete obj[key];
            else if (typeof obj[key] == "object" && key == '$date') {
                if (!obj[key]._isValid) delete obj[key];
            } else if (typeof obj[key] == "object") {
                this.removeEmpty(obj[key]);
            } if (typeof obj[key] == "object" && obj[key] && Object.keys(obj[key]).length === 0) { delete obj[key]; }
        };
        return obj;
    }

    public deleteNullElements(element) {
        for (let option in element) {
            if (element[option] === null) delete element[option];
        }
        return element;
    }

    public getKeysOfArray(array) {
        let result = []
        if (!array) return result;
        for (let element of array){
            if (!(element.key == null || typeof element.key == 'undefined')) result.push(element.key);
        }
        return result;
    }

    public getRequestOptions(filters, orders, page, pagesize) {
        const search = {
          filters: this.removeEmpty(filters),
          orders: this.removeEmpty(orders),
          page,
          pagesize
        };
    
        this.deleteNullElements(search);
        let params = new HttpParams();
        params = params.set('params', JSON.stringify(search));
        return params;
    }

}
