import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Subject, Observable, throwError } from 'rxjs';
import { timeout, catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { CommonService } from './common.service';
import { BehaviorSubject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ProductService {

  port = 1991;
  url = environment.apiUrl;
  timeout = 30000;
  leftBarFilters = {};
  public productsChangedState = new BehaviorSubject<any>({});

  constructor(public http: HttpClient, private commonService: CommonService) {}

   getProductosAPI(filters = null, orders = null, page = 0, pagesize = 0): Observable<any> {
    const params = this.commonService.getRequestOptions(filters, orders, page, pagesize);

    return this.http.get(this.url + '/api/' + 'productos', { params }).pipe(
      timeout(this.timeout),
      catchError((err) => this.handleError(err))
  )}

  getProducto(id: number): Observable<any> {
    return this.http.get(this.url + '/api/' + 'productos' + '/' + id).pipe(
      timeout(this.timeout),
      catchError((err) => this.handleError(err))
  )}

  handleError(err: HttpErrorResponse, hideError?: boolean) {
    return throwError(err);
  }


}
