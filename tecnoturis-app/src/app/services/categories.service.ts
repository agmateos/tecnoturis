import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { timeout, catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';


export interface Person {
  selectedCategory: any;
  selectedLvl1Subcategories: any;
  selectedLvl2Subcategories: any;
}

@Injectable({
  providedIn: 'root'
})

export class CategoriesService {

  port = 1991;
  url = environment.apiUrl;
  timeout = 30000;


  selectedCategory: any;
  selectedLvl1Subcategories: any;
  selectedLvl2Subcategories: any;
  
  public categoriesChangedState = new BehaviorSubject<any>({});
  constructor(public http: HttpClient) {}


  categoriesChanged(value) {
    this.categoriesChangedState.next(value);
  }
  clearCategories() {
    this.selectedLvl1Subcategories = null;
    this.selectedLvl2Subcategories = null;
    this.categoriesChangedState.next(null);
  }

  getSubcategories(categories) {
    let result = [];
    if (!categories) return result;
    if (!Array.isArray(categories)) categories = [categories];

    for (let category of categories) {
      if (category.subcategories) result = result.concat(category.subcategories);
    }

    return result;
  }

  getCategoryByValue(value, categories: any[]) {
    console.log(value);
    console.log(categories);
    if (value === false || !categories) return false;
    for (let category of categories) if (category.value == value) return category;
  }

  getParentCategoryByKey(key, categories, lookForOldestParent = false, parent = false) {
    if (!categories) return false;
    if (!Array.isArray(categories)) categories = [categories];

    if (parent) {
      for (let category of categories) {
        if (category.key == key) return parent;
      }
    }

    for (let category of categories) { // I do it in two loops for efficiency. Most likely chance is that the developper is providing the parent level categories as an attribute.
      if (category.subcategories) {
        parent = category;
        let subcategoriesResult = this.getParentCategoryByKey(key, category.subcategories, lookForOldestParent, parent);
        if (subcategoriesResult) return lookForOldestParent ? parent: subcategoriesResult; // If at somepoint we wanted to return grand parent (oldest parent in line) just return parent instead of subcategoriesResult;
      }
    }
    return false;
  }

  getCategoriesAPI(query?: any): Observable<any> {
    
   const params = query ? Object.keys(query).reduce((acc, key) => acc.set(key, query[key]), new HttpParams()) : new HttpParams();

   return this.http.get(this.url + '/api/' + 'categories', { params }).pipe(
     timeout(this.timeout),
     catchError((err) => this.handleError(err))
  )}





  handleError(err: HttpErrorResponse, hideError?: boolean) {
    return throwError(err);
  }
}
