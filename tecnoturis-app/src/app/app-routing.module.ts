import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component'
import { CatalogComponent } from './components/catalog/catalog.component'
import { ProductDetailedComponent } from './components/product-detailed/product-detailed.component'

export const routes: Routes = [
  /*{
    path: 'home',
    component: HomeComponent
  },*/
  { path: 'catalog', component: CatalogComponent },
  { path: 'catalog/product/:id', component: ProductDetailedComponent },
  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
