import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'atrezzo-app';

  
  constructor( public translate: TranslateService){
    let browserLang = translate.getBrowserLang();
    browserLang = 'es';
    if (browserLang == 'es') {
      translate.addLangs(['es','en']);
    } else {
      translate.addLangs(['en','es']);
    }

    //translate.addLangs(['es','en']);
    translate.setDefaultLang('es');
    console.log(browserLang);

    /******************ESTAS LÍNEAS DEFINEN EL IDIOMA************************ */
    //translate.use( 'es');
    translate.use(browserLang.match(/es|en/) ? browserLang: 'es');
  }
}
